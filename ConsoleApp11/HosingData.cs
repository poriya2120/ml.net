﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.ML.Data;

namespace ConsoleApp11
{
   internal class HosingData
    {
        [LoadColumn(0)]
        public float Longitude { get; set; }


        [LoadColumn(1)]
        public float Lititude { get; set; }

        [LoadColumn(2)]
        public float HousingMedianAge { get; set; }

        [LoadColumn(3)]
        public float TotalRooms { get; set; }


        [LoadColumn(4)]
        public float unkow { get; set; }


        [LoadColumn(5)]
        public float Populataion { get; set; }


        [LoadColumn(6)]
        public float Households { get; set; }

        [LoadColumn(7)]
        public float MedianIncome { get; set; }
        [LoadColumn(8)]
        public float MedianHouseValue { get; set; }
        [LoadColumn(9)]
        public float OceanProximity { get; set; }
    }
}
