﻿using Microsoft.ML;
namespace ConsoleApp11
{

    class Program
    {
        static void Main(string[] args)
        {
            var context = new MLContext();
            var data = context.Data.LoadFromTextFile<HousingData>("./housing.csv", hasHeader: true,
                separatorChar: ','
                );
            var split = context.Data.TrainTestSplit(data, testFraction: 0.2);

            var featuers = split.TrainSet.Schema.Select(col => col.Name)
                .where(colName => colName != "Label" && colName != "OceanProximity")
                .ToArray();

            var pipeline = context.Transforms.Text.FeaturizeText("Text", "OceanProximity")
                .Append(context.Transforms.Concatenate("features", featuers))
                                .Append(context.Transforms.Concatenate("features", "Features", "Text"))

                            .Append(context.Regression.Trainers.LbfgsPoissonRegression());
            var Model = pipeline.Fit(split.TrainSet);
            var Predictions = Model.Transform(split.TestSet);
            var Metrics = context.Regression.Evaluate(Predictions);

            Console.writeLine($"R^2 - {Metrics.RSquared}");


        }
    }
}
